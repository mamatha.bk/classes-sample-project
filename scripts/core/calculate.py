import math


class Circle:
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return math.pi * (self.radius ** 2)

    def peri(self):
        return 2 * math.pi * self.radius


class Rectangle:
    def __init__(self, length, breadth):
        self.length = length
        self.breadth = breadth

    def area(self):
        return self.length * self.breadth

    def per(self):
        return 2 * (self.length + self.breadth)


class Square:
    def __init__(self, length):
        self.length = length

    def area(self):
        return self.length * self.length

    def per(self):
        return 4 * self.length
