from scripts.core.calculate import Circle
from scripts.core.calculate import Rectangle
from scripts.core.calculate import Square

shape1 = input("Enter shape\n")
if shape1 == "circle":
    r = int(input("Enter radius"))
    circle_obj = Circle(r)
    circle_area = circle_obj.area()
    circle_per = circle_obj.peri()
    print(circle_area)
    print(circle_per)
elif shape1 == "rectangle":
    l3 = int(input("Enter length"))
    b = int(input("Enter breadth"))
    Rect_obj = Rectangle(l3, b)
    Rect_area = Rect_obj.area()
    Rect_per = Rect_obj.per()
    print(Rect_area)
    print(Rect_per)
elif shape1 == "square":
    l1 = int(input("Enter length:"))
    squ_obj = Square(l1)
    squ_area = squ_obj.area()
    squ_per = squ_obj.per()
    print(squ_obj)
    print(squ_per)
